import { Pipe, PipeTransform } from '@angular/core';
   @Pipe({name: 'Image'})
   export class ImagePipe implements PipeTransform {
     transform(value: any, arg) : any {
       let newVal: any;
       if (value) {
         newVal = "http://192.168.1.8/"+ value;
       } else{
         newVal ="http://www.quikpikz.com/images/default-logo.png"
       }
       return newVal;
     }
}