import { Injectable } from '@angular/core';
import { Http, Response, Headers } from '@angular/http';
import 'rxjs/add/operator/map';

/*
  Generated class for the MyRequest provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular 2 DI.
*/
@Injectable()
export class MyRequest {

    // baseUrl:string = 'http://jwt.local/api/';
    baseUrl:string = 'http://192.168.1.8/api/';


  constructor(public http: Http) {
console.log('hello ahmed')
  }

 //  getRemoteData(){
 // return this.http.get('https://jsonplaceholder.typicode.com/users').map(res=>res.json());
 //  }


  createAuthToken(headers : Headers){
      headers.append('Authorization','Bearer '+ localStorage.getItem('token'));
  }


    getRemoteData() {
      let headers =  new Headers();
      this.createAuthToken(headers);
      return this.http.get(this.baseUrl + 'users',{headers: headers}).map(res=>res.json());
}


    login(data){
        return this.http.post(this.baseUrl+"login_check", data).map(this.extractData);
    }

    isLogged(){
        if(window.localStorage.getItem('token')){
            return true
        }else{
            return false;
        }
    }

    logout(){
        window.localStorage.removeItem('token');
        return true;
    }

    private extractData(res: Response){
        let body = res.json();
        if(body.token){
             localStorage.setItem('token', body.token);
        };
        return body || {};
    }

}
