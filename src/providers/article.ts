import { Injectable } from '@angular/core';
import {Headers, Http, RequestOptions} from '@angular/http';
import 'rxjs/add/operator/map';
import {ToastController} from "ionic-angular";

/*
  Generated class for the Article provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular 2 DI.
*/
@Injectable()
export class ArticleProvider {
    // baseUrl:string = 'http://jwt.local/api/articles';
    baseUrl:string = 'http://192.168.1.8/api/articles';


  constructor(public http: Http,  public toastCtrl: ToastController) {
    console.log('Hello Article Provider');
  }

    createAuthToken(headers : Headers){
      headers.append('Authorization','Bearer '+ localStorage.getItem('token'));
    }


    getListArticle() {
      let headers =  new Headers();
      this.createAuthToken(headers);
      return this.http.get(this.baseUrl, {headers: headers}).map(res=>res.json());
    }

    getArticleById(id) {
      let headers =  new Headers();
      this.createAuthToken(headers);
      return this.http.get(this.baseUrl+ "/" + id, {headers: headers}).map(res=>res.json());
    }

    create(data){
      let headers =  new Headers();
      this.createAuthToken(headers);
      let options = new RequestOptions({ headers: headers });
      console.log(data)
      return this.http.post(this.baseUrl, data, options).map(res=>res.json());
    }

    delete(id){
      let headers =  new Headers();
      this.createAuthToken(headers);
      let options = new RequestOptions({ headers: headers });
      return this.http.delete(this.baseUrl+ "/" + id, options).map(res=>res.json());
  }

  edit(data, id){
      let headers =  new Headers();
      this.createAuthToken(headers);
      let options = new RequestOptions({ headers: headers });
      console.log(data)
      return this.http.put(this.baseUrl+ "/" + id, data, options).map(res=>res.json());
    }

    getListArticleByOffset(limit, offset) {
      let data = {"limit":limit, "offset":offset};
      let headers =  new Headers();
      this.createAuthToken(headers);
      let options = new RequestOptions({ headers: headers });
      options.headers.set('Limit', limit);
      console.log("data :", data)
      console.log("options :", options)
      return this.http.post(this.baseUrl+"/search", data, options).map(res=>res.json());
    }

    searchListArticleByOffset(data, limit, offset) {
      data["limit"] = limit;
      data["offset"] = offset;
      let headers =  new Headers();
      this.createAuthToken(headers);
      let options = new RequestOptions({ headers: headers });
      return this.http.post(this.baseUrl+"/search", data, options).map(res=>res.json());
    }

    searchArticleByOffset(data) {
      let headers =  new Headers();
      this.createAuthToken(headers);
      let options = new RequestOptions({ headers: headers });
      return this.http.post(this.baseUrl+"/search", data, options).map(res=>res.json());
    }

}
