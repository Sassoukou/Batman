import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';

import { AboutPage } from '../pages/about/about';
import { ContactPage } from '../pages/contact/contact';
import { HomePage } from '../pages/home/home';
import { TabsPage } from '../pages/tabs/tabs';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { MyRequest } from "../providers/my-request";
import { HttpModule } from "@angular/http";
import { LoginPage } from "../pages/login/login";
import { Dashboard } from "../pages/dashboard/dashboard";

import { ArticleProvider } from "../providers/article";
import { Article } from "../pages/article/article";
import { SampleModalPage } from "../pages/article/modals/sample-modal";
import {logout} from "../pages/myComponent/comp_logout/comp_logout";
import {ImagePipe} from "../utils/image-pipe";

import {DetailArticle} from "../pages/article/actions/details/details-article";
import {AddArticle} from "../pages/article/actions/add/add-article";
import {EditArticle} from "../pages/article/actions/edit/edit-article";


/*
* Importing Ladda Module 
*/
import { LaddaModule } from 'angular2-ladda';

@NgModule({
  declarations: [
    MyApp,
    AboutPage,
    ContactPage,
    Dashboard,
    HomePage,
    ImagePipe,
    DetailArticle,
    AddArticle,
    EditArticle,
    TabsPage,
    Article,
    logout,
    LoginPage,
    SampleModalPage
  ],
  imports: [
    BrowserModule,
    HttpModule,
    IonicModule.forRoot(MyApp),
    LaddaModule

  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    AboutPage,
    Dashboard,
    ContactPage,
    HomePage,
    TabsPage,
    DetailArticle,
    AddArticle,
    EditArticle,
    Article,
    logout,
    LoginPage,
    SampleModalPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},MyRequest,ArticleProvider
  ]
})
export class AppModule {}
