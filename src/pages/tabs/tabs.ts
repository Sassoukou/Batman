import { Component } from '@angular/core';

import { AboutPage } from '../about/about';
import { ContactPage } from '../contact/contact';
import { HomePage } from '../home/home';
import {Dashboard} from "../dashboard/dashboard";
import {Article} from "../article/article";
// import {LoginPage} from "../login/login";

@Component({
  templateUrl: 'tabs.html'
})
export class TabsPage {

  // tab0Root = LoginPage;
  tab0Root = Dashboard;
  tab1Root = HomePage;
  tab2Root = AboutPage;
  tab3Root = ContactPage;
  tab4Root = Article;

  constructor() {

  }
}
