import {Component} from '@angular/core';
import {NavParams, ToastController, ViewController} from 'ionic-angular';
import {ArticleProvider} from "../../../providers/article";


@Component({
    selector: 'page-sample-modal',
    templateUrl: 'sample-modal.html'
})
export class SampleModalPage {

    arts: any;
    listArts: any;
     isLoading: any ;
    constructor(public navParams: NavParams,
                public viewCtrl: ViewController,
                public toastCtrl: ToastController,
                private rest_article: ArticleProvider) {
        console.log(navParams)

        // let listArts = Object.assign({}, navParams.data);

        this.arts = navParams.data.arts;
        // this.listArts = Object.assign({}, navParams.data.listArts);
        this.listArts = navParams.data.listArts;
    }

    editArticle(art) {
        this.isLoading = true ;
        console.log(art)
        let data = {"wording": art.wording, "price": art.price}
        let index = this.listArts.indexOf(art);
        this.rest_article.edit(data, art.id).subscribe(data => {
            this.isLoading = false ;
            console.log(data);
            let toast = this.toastCtrl.create({
                message: 'Article was edit successfully',
                duration: 10000,
                cssClass: "toast-success"
            });
            toast.present();

            this.viewCtrl.dismiss();
            if (index > -1) {
                this.listArts[index].wording = art.wording;
                this.listArts[index].price = art.price;
            }
        }, error => {
            this.isLoading = false ;
            console.log(error)
            let code = error.status;
            let msg = JSON.parse(error._body).error.exception[0].message ;
            let toast = this.toastCtrl.create({
                message: 'Edit Article \n ' +code +' : '+msg,
                duration: 10000,
                cssClass: "toast-error"
            });
            toast.present();


        })
    }

    closeModal() {
        this.viewCtrl.dismiss();
    }
}