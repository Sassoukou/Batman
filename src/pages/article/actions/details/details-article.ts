import {Component} from '@angular/core';
import {NavParams} from "ionic-angular";

/**
 * Generated class for the Article page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@Component({
    selector: 'details-article',
    templateUrl: 'details-article.html',
})


export class DetailArticle {
    // listArt: String;
    article: any;


    constructor(public navParams: NavParams) {
this.article = navParams.get('article');
    }



    ionViewDidLoad() {

    }


}

 