import {Component, Input} from '@angular/core';
import {NavParams, NavController, ToastController} from "ionic-angular";
import {Camera} from 'ionic-native';
import {ArticleProvider} from "../../../../providers/article";
import { Article } from "../../article";


   

/**
 * Generated class for the Article page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@Component({
    selector: 'edit-article',
    templateUrl: 'edit-article.html',
})


export class EditArticle {
    arts: any;
    listArts: any;
     isLoading: any ;
    constructor(public navParams: NavParams,
                public toastCtrl: ToastController,
                private rest_article: ArticleProvider,
                public navCtrl: NavController) {
        console.log(navParams)

        // let listArts = Object.assign({}, navParams.data);

        this.arts = navParams.data.article.arts;
        // this.listArts = Object.assign({}, navParams.data.listArts);
        this.listArts = navParams.data.article.listArts;
    }

    editArticle(art) {
        this.isLoading = true ;
        console.log(art)
        let data = {"wording": art.wording, "price": art.price}
        let index = this.listArts.indexOf(art);
        this.rest_article.edit(data, art.id).subscribe(data => {
            this.isLoading = false ;
            console.log(data);
            let toast = this.toastCtrl.create({
                message: 'Article was edit successfully',
                duration: 10000,
                cssClass: "toast-success"
            });
            toast.present();

            if (index > -1) {
                this.listArts[index].wording = art.wording;
                this.listArts[index].price = art.price;
            }
            this.navCtrl.pop();
        }, error => {
            this.isLoading = false ;
            this.navCtrl.pop();
            console.log(error)
            let code = error.status;
            let msg = JSON.parse(error._body).error.exception[0].message ;
            let toast = this.toastCtrl.create({
                message: 'Edit Article \n ' +code +' : '+msg,
                duration: 10000,
                cssClass: "toast-error"
            });
            toast.present();


        })
    }

    ionViewDidLoad() {

    }


}

 