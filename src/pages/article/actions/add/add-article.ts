import {Component, Input} from '@angular/core';
import {NavParams, NavController, ToastController} from "ionic-angular";
import {Camera} from 'ionic-native';
import {ArticleProvider} from "../../../../providers/article";
import { Article } from "../../article";


   

/**
 * Generated class for the Article page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@Component({
    selector: 'add-article',
    templateUrl: 'add-article.html',
})


export class AddArticle {
    // listArt: String;
    public base64Image: string;
    public editLoader: boolean = false;
    constructor(private rest_article: ArticleProvider,
                 public navParams: NavParams,
                public toastCtrl: ToastController,
                 public navCtrl: NavController) {

    }

	getPicture(){
        Camera.getPicture({
            destinationType: Camera.DestinationType.DATA_URL,
            targetWidth: 1000,
            targetHeight: 1000
        }).then((imageData) => {
            // imageData is a base64 encoded string
            this.base64Image = "data:image/jpeg;base64," + imageData;
        }, (err) => {
            console.log(err);
        });
    }
    sendImage(arts){

	    this.editLoader =  true;
        let data = {"wording" : arts.wording , "price" : arts.price , "imageData":this.base64Image}
        this.rest_article.create(data).subscribe(data => {
	    this.editLoader =  false;

            let toast = this.toastCtrl.create({
                message: 'Article was added successfully',
                duration: 10000,
                cssClass: "toast-success"
            });
            toast.present();


            console.log(data)
            this.navCtrl.pop();
        }, error =>{
	    this.editLoader =  false;
            console.log(error)
            let code = error.status;
            let msg = JSON.parse(error._body).error.exception[0].message;
            let toast = this.toastCtrl.create({
                message: 'Add Article \n ' + code + ' : ' + msg,
                duration: 10000,
                cssClass: "toast-error"
            });
            toast.present();

        })
    }

    ionViewDidLoad() {

    }


}

 