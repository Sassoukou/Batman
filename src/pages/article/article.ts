import {Component, Renderer} from '@angular/core';
import {
    ModalController, IonicPage, NavController, NavParams, ToastController, LoadingController,
    AlertController
} from 'ionic-angular';
import {ArticleProvider} from "../../providers/article";

import {SampleModalPage} from "../article/modals/sample-modal";
import {DetailArticle} from "../article/actions/details/details-article";
import {AddArticle} from "./actions/add/add-article";
import {EditArticle} from "./actions/edit/edit-article";

/**
 * Generated class for the Article page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
    selector: 'page-article',
    templateUrl: 'article.html',
})


export class Article {
    // listArt: String;
    listArts: any;
    Deleteloading: any;
    public offset : any = 1;
    public _isLoading: boolean = false;

    constructor(public navCtrl: NavController,
                public navParams: NavParams,
                private rest_article: ArticleProvider,
                public toastCtrl: ToastController,
                public modalCtrl: ModalController,
                private alertCtrl: AlertController,
                public loadingCtrl: LoadingController,
                public renderer: Renderer) {

        if(this.navParams.get('isLoading')){
            this._isLoading = this.navParams.get('isLoading')
        }
    }


   // public get isLoading(): boolean {
   //      return this._isLoading;
   //  }
   //
   // public set isLoading(value: boolean) {
   //      this._isLoading = value;
   //  }


    addNewArticle() {
        console.log('FormArticle.value');
        this.navCtrl.push(AddArticle);
        this._isLoading = true;
    }

    deleteArticle(art) {
        let alert = this.alertCtrl.create({
            title: 'Confirm Delete',
            message: 'Do you want to delete this article?',
            buttons: [
                {
                    text: 'Cancel',
                    role: 'cancel',
                    handler: () => {
                        console.log('Cancel clicked');
                    }
                },
                {
                    text: 'Yes',
                    handler: () => {
                        this.Deleteloading = true;

                        console.log(art)
                        let index = this.listArts.indexOf(art);
                        this.rest_article.delete(art.id).subscribe(data => {
                            this.Deleteloading = false;
                            if (index > -1) {
                                this.listArts.splice(index, 1);
                                let toast = this.toastCtrl.create({
                                    message: 'Article was delete successfully',
                                    duration: 10000,
                                    cssClass: "toast-success"
                                });
                                toast.present();
                            }
                        }, error => {
                            this.Deleteloading = false;

                            let code = error.status;
                            let msg = JSON.parse(error._body).error.exception[0].message;
                            let toast = this.toastCtrl.create({
                                message: 'Delete Article \n ' + code + ' : ' + msg,
                                duration: 10000,
                                cssClass: "toast-error"
                            });
                            toast.present();
                        })


                    }
                }
            ]
        });
        alert.present();
    }

    detailsArticle(article, slidingItem){
        this.navCtrl.push(DetailArticle,{'article':article})
        // slidingItem.close();
    }

    editArticle(data, slidingItem) {
        let data2 = {"arts": data, "listArts": this.listArts}
        this.navCtrl.push(EditArticle,{'article':data2})
        // slidingItem.close();
        // let articleModal = this.modalCtrl.create(SampleModalPage, data2);
        // articleModal.present();
    }

    doRefresh(refresher) {
        this.rest_article.getListArticleByOffset(15, 1).subscribe(data => {
            console.log('succcccc', data);
            this.listArts = data;
            refresher.complete();
        }, error => {
            let code = error.status;
            let msg = JSON.parse(error._body).error.exception[0].message;
            let toast = this.toastCtrl.create({
                message: 'List Article \n ' + code + ' : ' + msg,
                duration: 10000,
                cssClass: "toast-error"
            });
            toast.present();
        })
    }


    doInfinite(infiniteScroll) {
    console.log('Begin async operation');

    setTimeout(() => {
      this.rest_article.getListArticleByOffset(5, this.offset).subscribe(res => {
            this.offset ++;
            for (let arts of res) {
            this.listArts.push(arts)
          }
        })
      /*if (res.lenght()) {
          infiniteScroll.complete();
      }*/
      infiniteScroll.complete();
    }, 1000)
}
    /*ionViewLoaded() {
        this.randomUsr.loadUser().subscribe(res => this.users = res.results)
    }*/
    ionViewDidLoad() {
        let loader = this.loadingCtrl.create({
            showBackdrop: false,
            spinner: 'ios'
        });
        loader.present();
        this.rest_article.getListArticleByOffset(15, 1).subscribe(data => {
            loader.dismiss();
            console.log('succcccc', data);
            this.listArts = data;

        }, error => {


            console.log('error', error);
            
            loader.dismiss();
            let code = error.status;
            let msg = JSON.parse(error._body).error.exception[0].message;
            let toast = this.toastCtrl.create({
                message: 'List Article \n ' + code + ' : ' + msg,
                duration: 10000,
                cssClass: "toast-error"
            });
            toast.present();
        })
    }
}

 