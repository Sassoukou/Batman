import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {LoginPage} from "../login/login";
import {MyRequest} from "../../providers/my-request";

/**
 * Generated class for the Dashboard page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-dashboard',
  templateUrl: 'dashboard.html',
})
export class Dashboard {

  constructor(public navCtrl: NavController, private auth2: MyRequest) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad Dashboard');
  }



  logout(){
    this.auth2.logout();
    this.navCtrl.setRoot(LoginPage);
  }
}
