
import { Camera } from 'ionic-native';
import {Component} from "@angular/core";
import {ArticleProvider} from "../../providers/article";
@Component({
    selector: 'page-home',
    templateUrl: 'home.html'
})
export class HomePage {
    public base64Image: string;

    constructor(private rest_article: ArticleProvider) {

    }

    getPicture(){
        Camera.getPicture({
            destinationType: Camera.DestinationType.DATA_URL,
            targetWidth: 1000,
            targetHeight: 1000
        }).then((imageData) => {
            // imageData is a base64 encoded string
            this.base64Image = "data:image/jpeg;base64," + imageData;
        }, (err) => {
            console.log(err);
        });
    }


    sendImage(arts){
        // alert(arts)
        // alert(this.base64Image)
        let data = {"wording" : arts.wording , "price" : arts.price , "imageData":this.base64Image}
        // let index = this.listArts.indexOf(art);
        this.rest_article.create(data).subscribe(data => {
            console.log(data)
            // if(index > -1){
            //     this.listArts[index].wording = art.wording;
            //     this.listArts[index].price = art.price;
            // }
        }, error =>{
            console.log(error)

        })
    }
}

