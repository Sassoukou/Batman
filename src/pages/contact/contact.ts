import {Component} from '@angular/core';
import {NavController} from 'ionic-angular';
import {MyRequest} from "../../providers/my-request";


@Component({
    selector: 'page-contact',
    templateUrl: 'contact.html'
})
export class ContactPage {
    listUsers: any;

    constructor(public navCtrl: NavController, public users: MyRequest) {
        this.users.getRemoteData().subscribe(data => {
            this.listUsers = data;
        });
    }

    details(user){
// this.navCtrl.push(RedditsPage,{user:user});

    }

}
