import {Component} from '@angular/core';
import {NavController} from 'ionic-angular';
import {MyRequest} from "../../providers/my-request";

@Component({
    selector: 'page-about',
    templateUrl: 'about.html',
})
export class AboutPage {

    constructor(public navCtrl: NavController, public req: MyRequest) {
        this.req.getRemoteData().subscribe(data=>{
            console.log('log data',data);
        });

    }


}
