import { Component, OnInit  } from '@angular/core';
import {LoadingController, NavController, ToastController} from 'ionic-angular';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AlertController } from 'ionic-angular';
import { MyRequest } from "../../providers/my-request";
import { TabsPage } from "../tabs/tabs";
// import * as _ from 'lodash';
/*
 Generated class for the Login page.
 See http://ionicframework.com/docs/v2/components/#navigation for more info on
 Ionic pages and navigation.
 */
@Component({
  selector: 'page-login',
  templateUrl: 'login.html'
})
export class LoginPage implements OnInit {
  form: FormGroup;
  logged: boolean = false;
  isLoading: boolean = false;
  constructor(
      public navCtrl: NavController,
      private auth: MyRequest,
      public alertCrtl: AlertController,
      public toastCtrl: ToastController,
      public loadingCtrl: LoadingController,
       public fb: FormBuilder,
  ) {}

  ngOnInit() {
    this.form  = this.fb.group({
      username: ['ismail', Validators.required],
      password: ['ismail', Validators.required]
    });
  }

  login({ value, valid }){
    this.isLoading = true;
   this.logged = true
    this.auth.login(value).subscribe(data => {
    this.isLoading = false;
      if(data.token){
        this.navCtrl.setRoot(TabsPage);
        this.logged = false
      }else{
        value.password = '';
        let alert = this.alertCrtl.create({
          title: 'Login Failed',
          subTitle: data.message,
          buttons: ['OK']
        })
        alert.present();
      }
    }, error =>{
    this.isLoading = false;

      let code = error.status;
      let msg = JSON.parse(error._body).error.exception[0].message ;
      let toast = this.toastCtrl.create({
        message: 'Login \n ' +code +' : '+msg,
        duration: 10000,
        cssClass: "toast-error"
      });
      toast.present();

    })
  }

  ionViewDidLoad() {
    console.log('Hello LoginPage Page');
  }

}
