import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

import {MyRequest} from "../../../providers/my-request";
import {LoginPage} from "../../login/login";

@Component({
  selector: 'logout',
  templateUrl: 'comp_logout.html'
})
export class logout {

  constructor(public navCtrl: NavController, private auth2: MyRequest) {

  }


  logout(){
   this.auth2.logout();
    this.navCtrl.setRoot(LoginPage);
  }
}
